package org.openjfx.repaso0;

public class IdiomasNoAbstract extends Idiomas{
    public IdiomasNoAbstract(int numeroHabitantes, String nombre) {
        super(numeroHabitantes, nombre);
    }
    @Override
    protected String saludar() {
        return palabras.get(0);
    }
    
    @Override
    protected String despedir() {
        return palabras.get(1);
    }
}
