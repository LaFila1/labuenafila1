package org.openjfx.repaso0;

import java.util.ArrayList;

public class Entrada {
    protected static ArrayList<IdiomasNoAbstract> idiomas;
    protected static ArrayList<Dialecto> dialectos;
    
    public static void main(String[] args) {
        idiomas = new ArrayList<IdiomasNoAbstract>();
        dialectos = new ArrayList<Dialecto>();
        start();
        getSaludosYDespedidas(true);
        getSaludosYDespedidas(false);
        getTodosLosIdiomas();
        crearDialecto();
        mostrarHablantesDialecto("Catalan");
    }
    
    protected static void start() {
        idiomas.add(new IdiomasNoAbstract(6, "castellano"));
        idiomas.get(0).ingresarPalabra("hola");
        idiomas.get(0).ingresarPalabra("adios");
        idiomas.add(new IdiomasNoAbstract(6, "ingles"));
        idiomas.get(1).ingresarPalabra("hello");
        idiomas.get(1).ingresarPalabra("bye");
        idiomas.add(new IdiomasNoAbstract(6, "aleman"));
        idiomas.get(2).ingresarPalabra("deutch");
        idiomas.get(2).ingresarPalabra("deutchland");
    }
    
    protected static void getSaludosYDespedidas(boolean control) {
        for (int i = 0; i < idiomas.size(); i++) {
            if (control)
                System.out.println("El saludo en " + idiomas.get(i).getNombre() + " es " + idiomas.get(i).getSaludo());
            else
               System.out.println("La despedida en " + idiomas.get(i).getNombre() + " es " + idiomas.get(i).getDespedida()); 
        }
    }
    
    protected static void getTodosLosIdiomas() {
        for (int i = 0; i < idiomas.size(); i++) 
            System.out.println(idiomas.get(i).getNombre());
    }
    
    protected static void crearDialecto() {
        dialectos.add(new Dialecto(idiomas.get(0), "Catalan"));
        dialectos.get(0).cuantificarHablantes(2);
    }
    
    protected static void mostrarHablantesDialecto(String nombreDialecto) {
        for (int i = 0; i < dialectos.size(); i++) {
            if (dialectos.get(i).getNombre().equalsIgnoreCase(nombreDialecto))
                System.out.println("El numero de habitantes del " + nombreDialecto + " es " + dialectos.get(i).getHablantes());
        }
    }
    
}
