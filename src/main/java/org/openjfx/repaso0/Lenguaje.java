package org.openjfx.repaso0;

public abstract class Lenguaje implements Cuantificable {
    protected IdiomasNoAbstract idioma;
    protected int numeroHablantes;
    protected String nombre;
    
    public Lenguaje(IdiomasNoAbstract idioma, String nombre) {
        setIdioma(idioma);
        setNombre(nombre);
    }
    
    protected void setIdioma(IdiomasNoAbstract idioma) {
        this.idioma = idioma;
    }
    
    @Override
    public void cuantificarHablantes(int n) {
        numeroHablantes = n;
    }
    
    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    protected String getNombre() {
        return nombre;
    }
    
    protected int getHablantes() {
        return numeroHablantes;
    }
}
