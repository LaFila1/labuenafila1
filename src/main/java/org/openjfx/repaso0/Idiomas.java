package org.openjfx.repaso0;

import java.util.ArrayList;

public abstract class Idiomas {
    protected int numeroHablantes;
    protected String nombre;
    protected ArrayList<String> palabras;
    
    public Idiomas(int numeroHabitantes, String nombre) {
        palabras = new ArrayList<String>();
        setNumeroHabitantes(numeroHabitantes);
        setNombre(nombre);
    }
    
    protected void setNumeroHabitantes(int numeroHabitantes) {
        this.numeroHablantes = numeroHablantes;
    }
    
    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    protected String getNombre() {
        return nombre;
    }
    
    protected void ingresarPalabra(String palabra) {
        palabras.add(palabra);
    }
    
    protected void listarPalabras() {
        String todasLasPalabras = "";
        for (int i = 0; i < palabras.size(); i++)
           todasLasPalabras += palabras.get(i);
        System.out.println("Las palabras en " + nombre + " son " + todasLasPalabras);
    }
    
    protected abstract String saludar();
    protected abstract String despedir();
    
    protected String getSaludo() {
        return palabras.get(0);
    }
    
    protected String getDespedida() {
        return palabras.get(1);
    }
}

